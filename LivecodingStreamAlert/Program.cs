﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace LivecodingStreamAlert
{
    class Program
    {
        const string CLIENT_ID = "";
        const string CLIENT_SECRET = "";
        static Token TOKEN;

        //Alert
        static string AlertUser { get; set; }
        static string AlertCategory { get; set; }
        static string AlertDifficulty { get; set; }
        static string AlertLanguage { get; set; }

        static void Main(string[] args)
        {
            Console.WriteLine("Livecoding.tv Stream Alert 0.1.1 © Urgau 2016 MIT License\n");

            //Args
            if (args.Length > 0)
            {
                for (int i = 0; i < args.Length; i += 2)
                {
                    switch (args[i].ToLower())
                    {
                        case "-user":
                            AlertUser = args[i + 1];
                            break;
                        case "-category":
                            AlertCategory = args[i + 1];
                            break;
                        case "-difficulty":
                            AlertDifficulty = args[i + 1];
                            break;
                        case "-language":
                            AlertLanguage = args[i + 1];
                            break;
                        default:
                            Console.WriteLine(string.Format("{0} don't exist !", args[i]));
                            break;
                    }
                }
            }

            //Code
            string urlAccesCode = "https://www.livecoding.tv/o/authorize/?" + string.Format("client_id={0}&response_type=code&state={1}&redirect_uri=http://localhost&scope=read", CLIENT_ID, Guid.NewGuid().ToString("N"));
            System.Diagnostics.Process.Start(urlAccesCode);

            Console.Write("Enter the code : ");
            string CODE = Console.ReadLine();

            //Token
            using (var client = new WebClient())
            {
                var values = new System.Collections.Specialized.NameValueCollection();
                values["grant_type"] = "authorization_code";
                values["code"] = CODE;
                values["redirect_uri"] = "http://localhost";

                //Auth
                string authInfo = CLIENT_ID + ":" + CLIENT_SECRET;
                authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
                client.Headers["Authorization"] = "Basic " + authInfo;

                var response = client.UploadValues("https://www.livecoding.tv/o/token/", values);
                var responseString = Encoding.Default.GetString(response);

                TOKEN = JsonConvert.DeserializeObject<Token>(responseString);
            }

            //Alert
            if (TOKEN != null)
            {
                while (true)
                {
                    Console.Clear();
                    Console.WriteLine("Livecoding.tv Stream Alert 0.1.1 © Urgau 2016 MIT License\n");
                    Console.WriteLine("Last check " + DateTime.Now.ToString("HH:mm:ss"));
                    using (WebClient webclient = new WebClient())
                    {
                        //Auth
                        webclient.Headers["Authorization"] = TOKEN.token_type + " " + TOKEN.access_token;

                        string reponse = webclient.DownloadString("https://www.livecoding.tv:443/api/livestreams/onair/");
                        Streams streams = JsonConvert.DeserializeObject<Streams>(reponse);

                        foreach (StreamDetail sd in streams.results)
                        {
                            //Verif alert
                            if (!string.IsNullOrWhiteSpace(AlertCategory) && sd.coding_category != AlertCategory)
                            {
                                continue;
                            }
                            if (!string.IsNullOrWhiteSpace(AlertLanguage) && sd.language != AlertLanguage)
                            {
                                continue;
                            }
                            if (!string.IsNullOrWhiteSpace(AlertDifficulty) && sd.difficulty != AlertDifficulty)
                            {
                                continue;
                            }
                            if (!string.IsNullOrWhiteSpace(AlertUser) && sd.user__slug != AlertUser)
                            {
                                continue;
                            }

                            WriteStreamDetail(sd);
                        }
                    }

                    System.Threading.Thread.Sleep(60000);
                }
            }

            Console.ReadKey();
        }

        static void WriteStreamDetail(StreamDetail sd)
        {
            Console.Beep();
            Console.WriteLine("---------------------------------------");
            Console.WriteLine("User : {0}", sd.user__slug);
            Console.WriteLine("Title : {0}", sd.title);
            //Console.WriteLine("Description : {0}", sd.description);
            Console.WriteLine("Category : {0}", sd.coding_category);
            Console.WriteLine("---------------------------------------");
        }

        class Streams
        {
            public int count { get; set; }
            public string next { get; set; }
            public string previous { get; set; }
            public List<StreamDetail> results { get; set; }
        }

        class StreamDetail
        {
            public string url { get; set; }
            public string user { get; set; }
            public string user__slug { get; set; }
            public string title { get; set; }
            public string description { get; set; }
            public string coding_category { get; set; }
            public string difficulty { get; set; }
            public string language { get; set; }
            public string tags { get; set; }
            public bool is_live { get; set; }
            public int viewers_live { get; set; }
            public string thumbnail_url { get; set; }
        }

        class Token
        {
            public string access_token { get; set; }
            public string token_type { get; set; }
            public int expires_in { get; set; }
            public string refresh_token { get; set; }
            public string scope { get; set; }
        }
    }
}
